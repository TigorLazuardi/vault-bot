FROM node:14.6-alpine3.12 AS builder-dev
WORKDIR /app
ENV SLACK_TOKEN=""
COPY tsconfig.json .
COPY package.json .
RUN npm install --production --silent
COPY src src/
CMD ["npm", "run", "start"]