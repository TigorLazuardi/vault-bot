# Vault Bot

Vault Bot is a slack bot to automate access to the Vault Hashicorp. The bot is designed to only generate access to Vault by generating, **not reading secrets** (Depending on the popularity this project get, more features my be added).

Creating / Adding secrets is still using vault UI / CLI until the app satisfied certain amount of security.

For now this is a very alpha, WIP project.
