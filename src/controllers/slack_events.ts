import { createEventAdapter } from '@slack/events-api';

const secret = process.env.SLACK_SIGNING_SECRET;

if (!secret) {
  console.error('Slack Signing Secret Required');
  process.exit(1);
}

const slackEvents = createEventAdapter(secret);

export default slackEvents;
