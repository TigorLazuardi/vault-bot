import { createMessageAdapter } from '@slack/interactive-messages';

const secret = process.env.SLACK_SIGNING_SECRET;

if (!secret) {
  console.error('Slack Signing Secret Required');
  process.exit(1);
}

const slackInteractions = createMessageAdapter(secret);

export default slackInteractions;
