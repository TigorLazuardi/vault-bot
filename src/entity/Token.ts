import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export default class Token extends BaseEntity {
  @CreateDateColumn({
    type: 'timestamp without time zone',
    name: 'created_at',
    default: new Date(),
  })
  // @ts-ignore
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp without time zone',
    name: 'updated_at',
    default: new Date(),
  })
  // @ts-ignore
  updatedAt: Date;

  @Column({ name: 'is_active', default: () => true })
  // @ts-ignore
  isActive: boolean;

  @Column({ name: 'is_expired', default: () => false })
  // @ts-ignore
  isExpired: boolean;

  checkExpiry(): this {
    if (new Date() > this.expireDate) {
      this.isExpired = true;
    }
    return this;
  }

  constructor(
    @PrimaryColumn()
    public token: string,

    @Column({ name: 'expire_date' })
    public expireDate: Date,
  ) {
    super();
  }
}
