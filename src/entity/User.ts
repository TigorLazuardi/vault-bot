import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import Token from './Token';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  // @ts-ignore
  public id: number;

  @CreateDateColumn({
    type: 'timestamp without time zone',
    name: 'created_at',
    default: new Date(),
  })
  // @ts-ignore
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp without time zone',
    name: 'updated_at',
    default: new Date(),
  })
  // @ts-ignore
  updatedAt: Date;

  @OneToMany((type) => Token, (token) => token.token)
  // @ts-ignore
  tokens: Token[];

  constructor(
    @Column({ name: 'username' })
    public userName: string,

    @Column({ name: 'slack_id' })
    public slackID: string,
  ) {
    super();
  }
}
