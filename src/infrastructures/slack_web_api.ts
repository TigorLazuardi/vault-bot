import { WebClient } from '@slack/web-api';

if (!process.env.SLACK_TOKEN) {
  console.error('SLACK_TOKEN required');
  process.exit(1);
}

const web = new WebClient(process.env.SLACK_TOKEN);

export default web;
