import Vault, { VaultOptions } from 'node-vault';

if (!process.env.VAULT_TOKEN) {
  console.error('Vault token required!');
  process.exit(1);
}

const options: VaultOptions = {
  apiVersion: 'v1',
  token: process.env.VAULT_TOKEN,
  endpoint: process.env.VAULT_HOST || 'http://127.0.0.1:8200',
};

const vault = Vault(options);

export default vault;
