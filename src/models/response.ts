export default interface ResponseBody<Data = {}, Err = string> {
  status: 'ok' | 'error';
  data: Data;
  error: Err;
  message: string;
}
