export interface VaultErrorResponse {
  response: {
    statusCode: number;
    body: {
      errors: [];
    };
  };
}
