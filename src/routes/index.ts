import { Router } from 'express';
import ResponseBody from '../models/response';
import slackRouter from './slack';
import vaultRouter from './vault';

const router = Router();

router.get<any, ResponseBody>('/', (r, w, next) => {
  w.json({
    error: '',
    status: 'ok',
    data: {},
    message: 'Server is alive',
  });
});

router.use('/vault', vaultRouter);
router.use('/slack', slackRouter);

router.use<any, ResponseBody>('*', (r, w, next) => {
  w.status(400).json({
    error: `no route handling for ${r.baseUrl} with method ${r.method}`,
    status: 'error',
    data: {},
    message: 'invalid uri path',
  });
});

export default router;
