import { Router } from 'express';
import slackEvents from '../controllers/slack_events';
import slackInteractions from '../controllers/slack_interactives';

const slackRouter = Router();

slackRouter.use('/interactives', slackInteractions.expressMiddleware());
slackRouter.use('/events', slackEvents.expressMiddleware());

export default slackRouter;
