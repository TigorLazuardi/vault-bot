import { Router } from 'express';
import vault from '../infrastructures/vault';
import ResponseBody from '../models/response';
import { VaultErrorResponse } from '../models/vault';

const vaultRouter = Router();

vaultRouter.get<{ config: string }, ResponseBody>(
  '/list/:config',
  async (r, w, next) => {
    try {
      const list = await vault.read(r.params.config);
      w.json({ data: list, error: '', message: 'OK', status: 'ok' });
    } catch (e) {
      const err = <VaultErrorResponse>e;
      w.status(err.response.statusCode).send(e);
    }
  },
);

vaultRouter.get<any, string[]>('/secrets', async (r, w, next) => {
  try {
    let endpoints: string[] = [];
    const mounts = await vault.mounts();
    const blacklist = ['cubbyhole/', 'identity/', 'sys/'];
    for (const key in mounts.data) {
      if (!blacklist.includes(key)) {
        endpoints.push(key);
      }
    }
    w.send(endpoints);
  } catch (e) {
    const err = <VaultErrorResponse>e;
    w.status(err.response.statusCode).send(e);
  }
});

export default vaultRouter;
