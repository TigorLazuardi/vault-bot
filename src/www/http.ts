if (process.env.NODE_ENV === 'develop') {
  require('dotenv').config();
}

import http from 'http';
import app from '../app';

const server = http.createServer(app);
const port = parseInt(process.env.PORT!) || 8080;

function close() {
  server.close((err) => {
    if (err) {
      console.error(err);
      process.exit(1);
    }
    process.exit(0);
  });
}

process.on('SIGTERM', close);
process.on('SIGINT', close);

server.listen(port, () => {
  console.log(`Server listens on port ${port}`);
});
